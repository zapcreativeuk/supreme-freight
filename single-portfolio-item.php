<?php 

    $landingTitle           = get_the_title();
    $customLandingTitle     = get_field( 'landing_custom_title' );
    $customLandingSubTitle  = get_field( 'landing_custom_sub_title' );
    $customLandingBtnLabel  = get_field( 'landing_button_label' );
    $landingHero            = get_the_post_thumbnail_url();
    $landingContent         = get_the_content();
    $landingExtraContent    = get_field( 'landing_extra_content' );
    $landingContentTitle    = get_field( 'landing_content_title' );
    $landingFeatured        = get_field( 'landing_featured_image' );
    $landingForm            = get_field( 'landing_modal_contact' );

    function landing_testimonials_styles() {
        wp_enqueue_style( 'supreme_freight_landing_testimonials', get_template_directory_uri() . '/dist/css/layout/component-testimonials.css', NULL, microtime() );
    }
    add_action( 'wp_enqueue_scripts', 'landing_testimonials_styles' );

    echo '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            ';
        ?>

        <!-- Global site tag (gtag.js) - Google Ads: 863801358 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-863801358"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'AW-863801358');
        </script>

        <script>
            gtag('config', 'AW-863801358/f0wYCIq7j5IBEI6g8psD', {
                'phone_conversion_number': '02380337778'
            });
        </script>
        
        <?php

            wp_head();
            
    echo '
        </head>
        <body class="sf-landing-page">

            <header class="d-flex justify-content-center py-4">
                ' . supremeFreightLandingLogo() . '
            </header>
            <main>

                <section class="sf-landing-page-hero" style="background: url(' . $landingHero . ') no-repeat center bottom; background-size: cover;">
                    
                    <div class="sf-landing-page-hero-overlay"></div>

                    <div class="container d-flex flex-column">
                        
                        <div class="row mt-auto">
                            
                            <div class="col-12 col-md-7 d-flex flex-column justify-content-end p-0 sf-white">
                                
                                <h1 class="sf-landing-page-hero-title mb-1">' . ( $customLandingTitle ? $customLandingTitle : 'Need a <span class="sf-green">Quick</span><br>' . $landingTitle . '<br>Quote?') . '</h1>

                            </div>
                            
                        </div>

                        <div class="row justify-content-center flex-column mt-3 mb-auto">
                            
                            <div class="col-12 col-md-5 d-flex flex-column justify-content-start p-0 sf-white">
                                <p class="sf-landing-page-hero-copy">' . ( $customLandingSubTitle ? $customLandingSubTitle : '<strong>We\'d love to help!</strong> All we need is a few<br class="d-none d-xl-block"> details for a quick and easy ' . $landingTitle . ' Quote, no matter what the cargo.' ) . '</p>
    
                                <button type="button" class="btn sf-landing-page-hero-button" data-toggle="modal" data-target="#exampleModalCenter">
                                    ' . ( $customLandingBtnLabel ? $customLandingBtnLabel : 'GET YOUR QUOTE NOW' ) . ' <i class="fas fa-chevron-right"></i>
                                </button>
                            </div>
                            
                        </div>

                    </div>

                </section>
            
                <section class="sf-landing-page-body py-3 py-md-5">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-5 mb-5 mb-md-2 my-lg-auto pr-l-5 order-2 order-md-1">
                                <img class="sf-landing-page-body-image mb-0" src="' . $landingFeatured['url'] . '" alt="">
                            </div>
                            <div class="col-12 col-md-5 order-1 order-md-2 mb-5 my-lg-auto">
                                <h2>Why Use Supreme?</h2>
                                    ' . $landingContent . '
                            </div>'
                            . ( $landingExtraContent ? '<div class="col-12 col-md-10 mx-auto my-lg-3 order-3">' . $landingExtraContent . '</div>' : $landingExtraContent ) . 
                        '</div>
                    </div>
                </section>
        
                <section class="sf-landing-page-accreds">
                    <div class="container">
                        <div class="row justify-content-center sf-landing-page-accreds"> 
                            <div class="col-12 col-md-10 d-flex flex-row justify-content-around align-items-center flex-wrap">
                                <h3 class="sf-landing-page-accreds-title">Proud to be accredited with:</h3>

                                <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0000_1000-CTIB.png"> -->
                                <img src="https://supremefreight.com/wp-content/uploads/0000_1000-CTIB.png">
                                <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0001_FIATA.png"> -->
                                <img src="https://supremefreight.com/wp-content/uploads/0001_FIATA.png">
                                <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0002_GLOBAL.png"> -->
                                <img src="https://supremefreight.com/wp-content/uploads/0002_GLOBAL.png">
                                <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0003_IATA.png"> -->
                                <img src="https://supremefreight.com/wp-content/uploads/0003_IATA.png">
                                <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0004_BIFA.png"> -->
                                <img src="https://supremefreight.com/wp-content/uploads/0004_BIFA.png">
                                <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0005_Solent-250.png"> -->
                                <img src="https://supremefreight.com/wp-content/uploads/0005_Solent-250.png">
                            </div>
                        </div>
                    </div>
                </section>
                
                <section class="testimonials px-0 pt-3 pt-md-5 align-items-center bg-green" style="padding-bottom: 0 !important;">

                    <div class="container px-0">
                        <div id="heroCarousel" class="carousel slide" data-ride="carousel" data-interval="8000">
                            <h3 class="testimonials-title">Testimonials</h3>

                            <!-- Indicators dot nav -->
                            <ol class="carousel-indicators">'; 
                                $indicator = 0;

                                    while( have_rows('carousel') ): the_row();
                                        if ($indicator === 0) {
                                            echo '<li data-target="#heroCarousel" data-slide-to="0" class="active"></li>';
                                        } else {
                                            echo '<li data-target="#heroCarousel" data-slide-to="'.$indicator.'"></li>';
                                        }
                                    $indicator++;
                                endwhile;
                            echo '   
                            </ol>

                            <div class="carousel-inner">';
                            
                                $slide = 0;

                                $testimonials = new WP_Query( array(
                                    'posts_per_page'    => 5,
                                    'post_type'         => 'testimonial'

                                ));

                                while( $testimonials->have_posts() ) :
                                    $testimonials->the_post();

                                    $exportTitle        = get_the_title();
                                    $exportLink         = get_the_content();

                            echo '
                                <div class="carousel-item ' . ( $slide === 0 ? 'active' : '' ) . '">
                                    <div class="carousel-caption">
                                        <div class="container px-0">
                                            <div class="row align-items-center justify-content-center">

                                                <div class="col-12 col-md-8">
                                                    ' . $exportLink . '
                                                </div>
                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ';
                            
                        $slide++;
                        endwhile;
                        wp_reset_postdata();
                echo '
                    </div>
                    <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                </div>
            </section>

            </main>
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 d-flex flex-column justify-content-center footer-left">
                            <p class="sf-white">
                                02380 337778
                            </p>
                            <p class="sf-green">
                                <a href="mailto:enquiries@supremefreight.com">enquiries@supremefreight.com</a>
                            </p>
                        </div>
                        <div class="col-12 footer-right col-md-6 d-flex flex-row">
                            <p>share this:</p>
                            <ul class="flex-row">
                                <li>
                                    <a href="https://twitter.com/intent/tweet" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/sharing/share-offsite/?url={url}" target="_blank">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=#url" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="@mailto:" target="_blank">
                                        <i class="far fa-envelope"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">   
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times"></i></span>
                            </button>
                        </div>
                        <div class="modal-body">
                            ' . $landingForm . '
                        </div>
                    </div>
                </div>
            </div>';
            
            wp_footer();
        echo '
        </body>
        </html>
    ';

?>