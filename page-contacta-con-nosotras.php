<?php

    $className = 'contact';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    

    get_header();

    $contactForm = get_field( 'contact_form' );

    echo '
        
        <div class="contact-map d-none d-md-block">
            <div class="" style="margin-top: 85px; display: flex; align-items: start; justify-content: center; position: relative;">
            
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-link active" id="nav-southampton-tab" data-toggle="tab" href="#nav-southampton" role="tab" aria-controls="nav-southampton" aria-selected="true">
                            Southampton Head Office
                        </a>

                        <a class="nav-link" id="nav-heathrow-tab" data-toggle="tab" href="#nav-heathrow" role="tab" aria-controls="nav-heathrow" aria-selected="false">
                            Heathrow Air Freight Office
                        </a>

                        <a class="nav-link" id="nav-felixstowe-tab" data-toggle="tab" href="#nav-felixstowe" role="tab" aria-controls="nav-felixstowe" aria-selected="false">
                            Felixstowe Port Office
                        </a>

                        <a class="nav-link" id="nav-la-tab" data-toggle="tab" href="#nav-la" role="tab" aria-controls="nav-la" aria-selected="false">
                            Los Angeles Office
                        </a>

                        <a class="nav-link" id="nav-hongkong-tab" data-toggle="tab" href="#nav-hongkong" role="tab" aria-controls="nav-hongkong" aria-selected="false">
                            Hong Kong Port Office
                        </a>

                        <a class="nav-link" id="nav-guangzhou-tab" data-toggle="tab" href="#nav-guangzhou" role="tab" aria-controls="nav-guangzhou" aria-selected="false">
                            Guangzhou Office
                        </a>

                        <a class="nav-link" id="nav-shenzhen-tab" data-toggle="tab" href="#nav-shenzhen" role="tab" aria-controls="nav-shenzhen" aria-selected="false">
                            Shenzhen Office
                        </a>
                        
                    </div>
                </nav>


                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-southampton" role="tabpanel" aria-labelledby="nav-southampton-tab">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9389.150660995732!2d-1.434236992687658!3d50.909639557683505!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487476975bfcf3f7%3A0x76dba34c780be48e!2sMillbrook%20Rd%20E%2C%20Southampton%20SO15%201JR!5e0!3m2!1sen!2suk!4v1632920508020!5m2!1sen!2suk" width="1600" height="400" style="width: 100vw; border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                    <div class="tab-pane fade" id="nav-heathrow" role="tabpanel" aria-labelledby="nav-heathrow-tab">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2487.213346990404!2d-0.5118608842326633!3d51.43587567962349!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876712e8a67cb31%3A0xd8ac025d0eb5fd60!2sMill%20Mead%2C%20Staines%20TW18%204UQ!5e0!3m2!1sen!2suk!4v1633422379880!5m2!1sen!2suk" width="1600" height="400" style="width: 100vw; border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                    <div class="tab-pane fade" id="nav-felixstowe" role="tabpanel" aria-labelledby="nav-felixstowe-tab">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d20672.38927255987!2d1.321811906533261!3d51.955460609966536!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d9777006708339%3A0x9a06b52e7ee10fb1!2sChaucer%20Rd%2C%20Felixstowe%20IP11%207RS!5e0!3m2!1sen!2suk!4v1633422794008!5m2!1sen!2suk" width="1600" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                    <div class="tab-pane fade" id="nav-la" role="tabpanel" aria-labelledby="nav-la-tab">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12055.219579434612!2d-116.31940675478843!3d33.75826948785262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dafbedc3486733%3A0xab457272b645e010!2s77530%20Enfield%20Ln%20h2%2C%20Palm%20Desert%2C%20CA%2092211%2C%20USA!5e0!3m2!1sen!2suk!4v1633422679255!5m2!1sen!2suk" width="1600" height="400" style="width" 100vw; border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                    <div class="tab-pane fade" id="nav-hongkong" role="tabpanel" aria-labelledby="nav-hongkong-tab">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d38288.42863582206!2d114.13146056142678!3d22.33278643493462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3404075455eae1c1%3A0x2270081219569b71!2sHong%20Kong%20Industrial%20Centre%20Block%20B%2C%20489-491%20Castle%20Peak%20Rd%2C%20Cheung%20Sha%20Wan%2C%20Hong%20Kong!5e0!3m2!1sen!2suk!4v1633422888540!5m2!1sen!2suk" width="1600" height="400" style="width: 100vw; border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                    <div class="tab-pane fade" id="nav-guangzhou" role="tabpanel" aria-labelledby="nav-guangzhou-tab">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d234552.67623010714!2d113.18998632168376!3d23.283607405686343!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3402e327650766cd%3A0x284013f7dc94a0be!2sBaiyun%2C%20Guangzhou%2C%20Guangdong%20Province%2C%20China!5e0!3m2!1sen!2suk!4v1633425889712!5m2!1sen!2suk" width="1600" height="400" style="width: 100vw;border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                    <div class="tab-pane fade" id="nav-shenzhen" role="tabpanel" aria-labelledby="nav-shenzhen-tab">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d62521.545611160385!2d114.08832224805896!3d22.578300150123777!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3403f5111c9cb897%3A0x657f7a5559205475!2s52%20Qingshuihe%201st%20Rd%2C%20Luohu%20Qu%2C%20Shenzhen%20Shi%2C%20Guangdong%20Sheng%2C%20China%2C%20518029!5e0!3m2!1sen!2suk!4v1633425817918!5m2!1sen!2suk" width="1600" height="400" style="width: 100vw; border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
        </div>' .

            supremeFreightPageTitleBanner( get_the_title() ) . 
            
        '<section class="' . $className . '">
            <div class="container">
                <div class="row">
                    <main class="col-12 col-md-8">
                        <div class="contact-form-wrapper">
                            <h2 class="contact-form-title">Realice ahora una consulta</h2>'
                            . $contactForm . '
                        </div>
                    '
                        . supremeFreightBreak() . 
                    '
                        <section class="row contact-team">
                            
                            <h2 class="contact-team-title">Póngase en contacto con uno de nuestro equipo directamente</h2>

                                <div class="row contact-team-list" style="padding: 0 1rem">';

                                $personnel = new WP_Query( array(
                                    'posts_per_page'    => -1,
                                    'post_type'         => 'personnel',
                                    'orderby'           => 'date',
                                    'order'             => 'ASC'
                
                                ));
                
                                while( $personnel->have_posts() ) :
                                    $personnel->the_post();
                
                                    $personnelName          = get_the_title();
                                    $personnelRole          = get_field( 'personnel_role' );
                                    $personnelEmail         = get_field( 'personnel_email' ); 
                                    $personnelPhone         = get_field( 'personnel_phone' );
                                    $personnelInt           = substr( $personnelPhone, 1 );
                                    $regex                  = '/(\\d{4})(\\d{3})/';
                                    $personnelIntOut        = preg_replace($regex, '$1 $2', $personnelInt);

                                    
                                    echo '
                                        <div class="col-12 col-md-4 contact-team-item">
                                            <div>
                                                <strong class="contact-team-item-title">' . $personnelName . '</strong><br>
                                                <span>' . $personnelRole . '</span><br>
                                                <a class="contact-team-item-email" href="mailto:' . $personnelEmail . '">' . $personnelEmail . '</a><br>
                                                (&#43;44)' . $personnelIntOut . '
                                            </div>
                                        </div>
                                    ';
                                
                                endwhile;
                                wp_reset_postdata();
                                
                                
                            echo '</div>
                            </ul>
                        </section>
                    </main>
                    <aside class="col-12 col-md-4">
                        <div class="contact-aside-offices">
                            <h3 class="contact-aside-offices-header">DETALLES DE NUESTRA DIRECCIÓN</h3>';

                            $office = new WP_Query( array(
                                'posts_per_page'    => -1,
                                'post_type'         => 'office',
                                'orderby'           => 'date',
                                'order'             => 'ASC'
            
                            ));
            
                            while( $office->have_posts() ) :
                                $office->the_post();
            
                                $officeName          = get_the_title();
                                $officeAddress       = get_field( 'office_address' );
                                $officePhone         = get_field( 'office_phone' );
                                $officeEmail         = get_field( 'office_email' ); 
                                
                                echo '
                                    <div class="contact-aside-office">
                                        <h4 class="contact-aside-office-title">' . $officeName . ':</h4>
                                        ' . ( $officeAddress ? '<p class="contact-aside-office-address">' . $officeAddress . '</p>' : '' ) . '
                                        <p class="contact-aside-office-contact">
                                            ' . ( $officePhone ? '<strong>T:</strong> ' . $officePhone . '<br>' : '' ) . '
                                            ' . ( $officeEmail ? '<strong>E:</strong> <a href="mailto:' . $officeEmail . '" class="contact-aside-office-contact-link">' . $officeEmail . '</a>' : '' ) . '
                                        </p>
                                    </div>
                                ';
                            
                            endwhile;
                            wp_reset_postdata();
                            
                    echo '
                        </div>
                    </aside>
                </div>
            </div>
        </section>
    ';
            
    get_footer();

?>