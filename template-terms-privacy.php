<?php

    /**
     * Template Name: Terms & Privacy
     */

    get_header();

    echo '
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10">
                    <section class="terms-privacy">';

                    echo '
                        <h1>Supreme Freight Services Limited ' . get_the_title() . '</h1>
                    ';

                        while( have_posts() ) :
                            the_post();    
                                the_content();                    
                        endwhile;

                        get_footer(); 

                echo '
                    </section>
                </div>
            </div>
        </div>
    ';

    get_footer();

