<?php

    function supreme_freight_resources() {
        // Scripts
        wp_enqueue_script( 'supreme_freight_dist_vendors', get_template_directory_uri() . '/dist/js/vendors.js', array(), false, true  );
        wp_enqueue_script( 'supreme_freight_dist_scripts', get_template_directory_uri() . '/dist/js/scripts.js', array(), false, true  );
        wp_enqueue_script( 'supreme_freight_dist_main', get_template_directory_uri() . '/dist/js/main.js', array(), false, true  );
        // Stylesheets
        wp_enqueue_style( 'supreme_freight_fontawesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css', NULL, microtime() );
        wp_enqueue_style( 'supreme_freight_bootstrap_styles', get_template_directory_uri() . '/dist/css/bootstrap.css', NULL, microtime() );
        wp_enqueue_style( 'supreme_freight_main_styles', get_template_directory_uri() . '/dist/css/main.css', NULL, microtime() );
        wp_enqueue_style( 'supreme_freight_styles', get_stylesheet_uri(), NULL, microtime() );
    }

    add_action( 'wp_enqueue_scripts', 'supreme_freight_resources' );

    function supreme_freight_features() {
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
        register_nav_menu( 'mainNav', 'Main Navigation' );
        register_nav_menu( 'headerMenu', 'Header Menu' );
        register_nav_menu( 'footerLegalMenu', 'Footer Legal Menu' );
    }

    add_action( 'after_setup_theme', 'supreme_freight_features' );

    function supreme_freight_widgets() {

        register_sidebar( array(
            'name'          => 'Single Sidebar',
            'id'            => 'single_sidebar',
            'after_widget'  => '<hr>'
        ) );

        register_sidebar( array(
            'name'          => 'Footer Contact',
            'id'            => 'footer_contact',
            'before_widget' => '<div class="footer-contact-info">',
            'after_widget'  => '</div>',
            'before_title'  => '<h6>',
            'after_title'   => '</h6>',
        ) );
    
        register_sidebar( array(
            'name'          => 'Footer Languages',
            'id'            => 'footer_languages',
            'before_widget' => '<div class="footer-contact-languages">',
            'after_widget'  => '</div>'
        ) );
        register_sidebar( array(
            'name'          => 'Footer Twitter',
            'id'            => 'footer_twitter'
        ) );
        register_sidebar( array(
            'name'          => 'Footer Legal',
            'id'            => 'footer_legal'
        ) );
        register_sidebar( array(
            'name'          => '404 Page',
            'id'            => 'page_404'
        ) );
    
    }
    add_action( 'widgets_init', 'supreme_freight_widgets' );

    /**
     *  Check if ACF is activated
     */
    if ( function_exists( 'acf_register_block_type' ) ) {
        /**
         * Adding specific ACF action
         */
        add_action( 'acf/init', 'register_acf_block_types' );
    }

    function register_acf_block_types() {

    /**
     * Content Blocks
     */

        /**
         * Carousel Block
         */
        acf_register_block_type(
            array(
                'name'              => 'carousel',
                'title'             => __( 'Carousel' ),
                'description'       => __( 'Supreme Carousel block' ),
                'render_template'   => 'inc/template-parts/blocks/carousel.php',
                'icon'              => 'format-gallery',
                'keywords'          => array(
                                        'carousel',
                                        'banner',
                                        'hero'
                                    )
                )
        );

        /**
         * General Content Block
         */
        acf_register_block_type(
            array(
                'name'              => 'general',
                'title'             => __( 'General Content' ),
                'description'       => __( 'General Content block' ),
                'render_template'   => 'inc/template-parts/blocks/component-general.php',
                'icon'              => 'edit',
                'keywords'          => array(
                                        'general',
                                        'generic'
                                    )
                )
        );

        /**
         * Blog Block
         */
        acf_register_block_type(
            array(
                'name'              => 'blog',
                'title'             => __( 'Blog Content' ),
                'description'       => __( 'Blog Content block' ),
                'render_template'   => 'inc/template-parts/blocks/component-blog.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-blog.css',
                'icon'              => 'edit',
                'keywords'          => array(
                                        'blog',
                                        'news'
                                    )
                )
        );

        /**
         * Carousel Block
         */
        acf_register_block_type(
            array(
                'name'              => 'hero',
                'title'             => __( 'Hero' ),
                'description'       => __( 'Supreme Hero Block' ),
                'render_template'   => 'inc/template-parts/blocks/component-hero.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-hero.css',
                'icon'              => 'cover-image',
                'keywords'          => array(
                                        'hero',
                                        'banner'
                                    )
                )
        );

        /**
         * Call to Action Block
         */
        acf_register_block_type(
            array(
                'name'              => 'call2action',
                'title'             => __( 'Call to Action' ),
                'description'       => __( 'Supreme Call to Action Block' ),
                'render_template'   => 'inc/template-parts/blocks/component-c2a.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-cta.css',
                'icon'              => 'megaphone',
                'keywords'          => array(
                                        'call to action',
                                        'contact details'
                                    )
                )
        );

        /**
         * Directors Block
         */
        acf_register_block_type(
            array(
                'name'              => 'directors',
                'title'             => __( 'Directors' ),
                'description'       => __( 'Supreme Directors Block' ),
                'render_template'   => 'inc/template-parts/blocks/component-directors.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-directors.css',
                'icon'              => 'megaphone',
                'keywords'          => array(
                                        'directors',
                                        'contact details'
                                    )
                )
        );

        /**
         * Intro
         */
        acf_register_block_type(
            array(
                'name'              => 'intro',
                'title'             => __( 'Intro' ),
                'description'       => __( 'Supreme intro block' ),
                'category'          => 'Supreme Freight',
                'render_template'   => 'inc/template-parts/blocks/component-intro.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-intro.css',
                'icon'              => 'id',
                'keywords'          => array(
                                        'intro',
                                        'text'
                                    )
                )
        );

        /**
         * iframe
         */
        acf_register_block_type(
            array(
                'name'              => 'iframe',
                'title'             => __( 'iframe' ),
                'description'       => __( 'Supreme iframe block' ),
                'category'          => 'Supreme Freight',
                'render_template'   => 'inc/template-parts/blocks/component-iframe.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-iframe.css',
                'icon'              => 'id',
                'keywords'          => array(
                                        'iframe'
                                    )
                )
        );

        /**
         * Services
         */
        /**
         * Import
         */
        acf_register_block_type(
            array(
                'name'              => 'import',
                'title'             => __( 'Import' ),
                'description'       => __( 'Supreme Import block' ),
                'category'          => 'Supreme Freight',
                'render_template'   => 'inc/template-parts/blocks/component-import.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-import-export.css',
                'icon'              => 'controls-back',
                'keywords'          => array(
                    'import',
                    'text'
                    )
                )
            );
        /**
         * Export
         */
        acf_register_block_type(
            array(
                'name'              => 'export',
                'title'             => __( 'Export' ),
                'description'       => __( 'Supreme Export block' ),
                'category'          => 'Supreme Freight',
                'render_template'   => 'inc/template-parts/blocks/component-export.php',
                'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-import-export.css',
                'icon'              => 'controls-forward',
                'keywords'          => array(
                    'export',
                    'text'
                    )
                )
            );
            /**
             * Page Header
             */
            acf_register_block_type(
                array(
                    'name'              => 'page-header',
                    'title'             => __( 'Page Header' ),
                    'description'       => __( 'Supreme Page Header' ),
                    'category'          => 'Supreme Freight',
                    'render_template'   => 'inc/template-parts/blocks/component-page-header.php',
                    'icon'              => 'sticky',
                    'keywords'          => array(
                                            'page header',
                                            'title'
                                        )
                    )
            );
    
            /**
             * Text Bar
             */
            acf_register_block_type(
                array(
                    'name'              => 'text-bar',
                    'title'             => __( 'Text Bar' ),
                    'description'       => __( 'Supreme Text Bar' ),
                    'category'          => 'Supreme Freight',
                    'render_template'   => 'inc/template-parts/blocks/component-textbar.php',
                    'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-textbar.css',
                    'icon'              => 'id',
                    'keywords'          => array(
                                            'intro',
                                            'text'
                                        )
                    )
            );
    
            /**
             * Testimonials Block
             */
            acf_register_block_type(
                array(
                    'name'              => 'testimonials',
                    'title'             => __( 'Testimonials' ),
                    'description'       => __( 'Supreme Testimonials' ),
                    'category'          => 'Supreme Freight',
                    'render_template'   => 'inc/template-parts/blocks/component-testimonial.php',
                    'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/component-testimonials.css',
                    'icon'              => 'id',
                    'keywords'          => array(
                                            'testimonials',
                                            'text'
                                        )
                    )
            );
            
            /**
             * Why Us Block
             */
            acf_register_block_type(
                array(
                    'name'              => 'why-us',
                    'title'             => __( 'Why Us' ),
                    'description'       => __( 'Why Us' ),
                    'category'          => 'Supreme Freight',
                    'render_template'   => 'inc/template-parts/blocks/component-whyus.php',
                    'enqueue_style'     => get_template_directory_uri() . '/dist/css/layout/why-us.css',
                    'icon'              => 'id',
                    'keywords'          => array(
                                            'why us',
                                            'why'
                                        )
                    )
            );
        }
