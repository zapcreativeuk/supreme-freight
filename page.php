<?php  
/**
 * Page template for the Supreme Freight SF034 theme.
 */

get_header();
$page_title = get_the_title();
?>
<section class="blog-header">
    <h2 class="blog-header-title"><?php echo wp_kses_post( $page_title ); ?></h2>
</section>
<?php 
while( have_posts() ) :
    the_post();    
        the_content();                    
endwhile;
?>
                    
<?php
get_footer(); 
?>

    