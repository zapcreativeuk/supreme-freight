<?php 
    
    /**
     * Front page template for the Supreme Freight v-2 Dev theme.
     */

    get_header();



    while( have_posts() ) :
        the_post();    
            the_content();                    
    endwhile;

    get_footer(); 
