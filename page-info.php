<?php

    $className = 'useful-info';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    get_header();

    echo 
        supremeFreightPageTitleBanner( get_the_title() ) . '

        <section class="' . $className . '">

            <div class="container useful-info">
                <div class="row">';
                    
                    if( have_rows('useful_items') ):
                        while( have_rows('useful_items') ) : the_row();
                    
                            $useful_item_name       = get_sub_field( 'useful_item_name' );
                            $useful_item_image      = get_sub_field( 'useful_item_image' );
                            $useful_item_button     = get_sub_field( 'useful_item_button' );
                            $useful_item_link       = get_sub_field( 'useful_item_link' );
                            $useful_external_link   = get_sub_field( 'useful_external_link' );

                            echo '
                                <div class="col-12 col-md-6">
                                    <div class="useful-info-item m-3">
                                        <div class="row">' . 
                                            ( 
                                            $useful_item_image ? 
                                            '<div class="col-6 useful-info-item-img">
                                                <img src="' . $useful_item_image['url'] . '" alt="' . $useful_item_image['alt'] . '">
                                            </div>
                                            <div class="col-6 useful-info-item-content">' :
                                            '<div class="col-12 useful-info-item-content align-items-center">'
                                            ) .
                                            '<p class="lead useful-info-item-title">' . $useful_item_name . '</p>
                                            <hr>
                                            <a href="' . $useful_item_link . '" target="_blank">
                                                <button class="btn btn-supreme useful-info-item-button">' . $useful_item_button . ( 
                                                    $useful_item_image ? 
                                                    ' <i class="fas fa-cloud-download-alt">' :
                                                    ''
                                                ) . '</i></button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ';
                    
                        endwhile;
                    endif;

                echo '    
                </div>
            </div>
            
        </section>
    ';
            
    get_footer();

?>
