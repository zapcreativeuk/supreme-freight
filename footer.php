<?php
    /**
     * Footer file for the wp-boilerplate.
     */

    echo ' 

        </main>
            <footer class="footer' . ( !is_front_page() ? ' footer-alt' : '' ) . '">

                <div class="container">
                    <div class="row">
                        
                        <div class="footer-contact col-12 col-md-6 col-lg-3">';
                            
                            echo supremeFreightLandingLogo();

                            echo '
                            <p>Supreme Freight Services Ltd<br>
                            Supreme House, Shirley Road,<br>
                            Southampton, SO15 3EW</p>
                        </div>
                        <div class="footer-twitter col-12 col-md-6 col-lg-3">';
                                
                            dynamic_sidebar( 'footer_contact' );
                            
                        echo'
                            </p>
                        </div>
                        <div class="footer-languages col-12 col-md-6 col-lg-3">';

                            dynamic_sidebar( 'footer_languages' );
                        
                    echo '
                        </div>
                        <div class="footer-legal col-12 col-md-6 col-lg-3">';

                            dynamic_sidebar( 'footer_legal' );
                        
                    echo '
                        </div>

                        

                    </div>
                
                    <div class="row footer-social-wrapper align-items-center justify-content-between py-3 py-md-0">
                        <div class="footer-social col-md-4">
                            Follow Us:
            
                            <ul class="footer-social-links">
                                <li class="footer-social-links-link">
                                    <a href="https://twitter.com/SupremeFreight" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="footer-social-links-link">
                                    <a href="https://www.linkedin.com/company/281470?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A281470%2Cidx%3A2-1-2%2CtarId%3A1475128859050%2Ctas%3Asupreme%20fre" target="_blank">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li class="footer-social-links-link">
                                    <a href="https://www.facebook.com/supremefreight" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li class="footer-social-links-link">
                                    <a href="https://supremefreight.com/" target="_blank">
                                        <i class="fab fa-skype"></i>
                                    </a>
                                </li>
                                <li class="footer-social-links-link">
                                    <a href="https://supremefreight.com/rss" target="_blank">
                                        <i class="fas fa-rss"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                            
                                <div class="footer-icons col-6 col-md-2 p-3">
                                    <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0005_Solent-250_v2.png" alt=""> -->
                                    <img src="https://supremefreight.com/wp-content/uploads/0005_Solent-250_v2.png" alt="">
                                </div>
                                <div class="footer-icons col-6 col-md-2 p-3">
                                    <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0004_BIFA_v2.png" alt=""> -->
                                    <img src="https://supremefreight.com/wp-content/uploads/0004_BIFA_v2.png" alt="">
                                </div>
                                <div class="footer-icons col-6 col-md-2 p-3">
                                    <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0002_GLOBAL_v2.png" alt=""> -->
                                    <img src="https://supremefreight.com/wp-content/uploads/0002_GLOBAL_v2.png" alt="">
                                </div>
                                <div class="footer-icons col-6 col-md-2 p-3">
                                    <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0000_1000-CTIB_v2.png" alt=""> -->
                                    <img src="https://supremefreight.com/wp-content/uploads/0000_1000-CTIB_v2.png" alt="">
                                </div>
                                <div class="footer-icons col-6 col-md-2 p-3">
                                    <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0001_FIATA_v2.png" alt=""> -->
                                    <img src="https://supremefreight.com/wp-content/uploads/0001_FIATA_v2.png" alt="">
                                </div>
                                <div class="footer-icons col-6 col-md-2 p-3 pr-0">
                                    <!-- <img src="' . get_theme_file_uri() . '/inc/img/_0003_IATA_v2.png" alt=""> -->
                                    <img src="https://supremefreight.com/wp-content/uploads/0003_IATA_v2.png" alt="">
                                </div>
                                
                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="sub-footer">
            <div class="container px-3">
                <div class="row">
                    <div class="col-md-6 p-0 text-center text-lg-left">
                        <small>©' . date('Y') . ' Supreme Freight Services Ltd</small>
                    </div>
                    <div class="col-md-6 p-0 pr-md-5 pr-xl-0 text-center text-lg-right">
                        <small>Company No: 02006494</small>
                    </div>
                </div>
            </div>
        </div>

        <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=web_widget/supremefreight.zendesk.com"></script>
        ' .  wp_footer() . '
            </body>
        </html>
    ';
?>