<?php 

    /**
    * Template Name: Import/Export Template
    */

    get_header();

    echo supremeFreightPageTitleBanner( get_the_title() );

    while( have_posts() ) :
        the_post();    
            the_content();                    
    endwhile;

    get_footer(); 
