<?php 

    $className = 'text-bar';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $textbarHeadline    = get_field( 'textbar_headline' );

    echo '
        <style>
            .text-bar-section {
                background: #1e3d74;
                color: #fff;
                padding-block: 1.875rem; }
                .text-bar-section h6 {
                margin: 0;
                font-family: "itc-avant-garde-gothic-pro", sans-serif;
                letter-spacing: 0.125rem; }
        </style>
        <section class="' . esc_attr( $className ) . '-section" style="background: #1e3d74;
        color: #fff;
        padding-block: 1.875rem;">
            <div class="container">

                <div class="row justify-content-center align-items center">
                    <h6 style="margin: 0;
                    font-family: \'itc-avant-garde-gothic-pro\', sans-serif;
                    letter-spacing: 0.125rem;
                    font-size: 1.25rem;
                    letter-spacing: 0.0625rem;">' . $textbarHeadline . '</h6>
                </div>

            </div>
        </section>
    ';

?>