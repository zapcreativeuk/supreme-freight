<?php
    /**
     * Hero block for the Supreme Freight v-2 Dev theme.
     */

    $className = 'hero-block';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $hero_Title              = get_field( 'hero_title' );
    $hero_BgImage            = get_field( 'hero_background_image' );
    $hero_Content            = get_field( 'hero_content' );
    $hero_Background         = get_field( 'hero_background_colour' );
    $hero_SubtitleColour     = get_field( 'hero_header_colour' );
    $hero_ContentColour      = get_field( 'hero_content_colour' );
?>

<section class="<?php 
        echo esc_attr( $className ); 
        echo ( $hero_Background ? ' supreme-bg-' . $hero_Background : '' );
    ?> ">

    <div class="hero-background" style="">
        <?php 
        if ( $hero_BgImage ) {
            echo sprintf(
                '<img src="%1$s" alt="%2$s" class="hero-background-image">',
                $hero_BgImage['url'],
                $hero_BgImage['alt']
            );
        } else {
            echo '<img src="https://supremefreight.com/wp-content/uploads/2023_image-1500x895-1.jpg" alt="" class="hero-background-image">';
        }
        ?>
        
        <div class="hero-background-overlay">

            <div class="container h-100">
                <div class="row h-100">
                    <div class="col d-flex justify-content-center align-items-center">
                        <?php 
                        if ( $hero_Title ) {
                            echo sprintf(
                                '<h1 class="hero-title text-center">%1$s</h1>',
                                wp_kses_post( $hero_Title )
                            );
                        }
                        ?>
                    </div>

                </div>
                
            </div>

        </div>

        
    </div>

</section>