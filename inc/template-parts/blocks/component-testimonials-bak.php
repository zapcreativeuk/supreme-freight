<?php 

    $className = 'testimonials';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $introHeader    = get_field( 'intro_header' );
    $introSubHeader = get_field( 'intro_sub-header' );

    echo '
        <section class="' . esc_attr( $className ) . '-section">
            <div class="container">
                <div class="row justify-content-center align-items-center text-center">
    ';

    // Check rows exists.
    if( have_rows('testimonials_carousel') ):

        // Loop through rows.
        while( have_rows('testimonials_carousel') ) : the_row();

            // Load sub field value.
            $testimonial = get_sub_field('testimonial');
            // Do something...
            echo $testimonial;

        // End loop.
        endwhile;

    // No value.
    else :
        // Do something...
    endif;

    
    echo '                
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="..." class="d-block w-100" alt="...">
                            </div>
                        <div class="carousel-item">
                            <img src="..." class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="..." class="d-block w-100" alt="...">
                        </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div>
            </div>
        </section>
    ';
?>


    