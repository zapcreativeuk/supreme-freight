<?php 

$className = 'testimonials';
if ( !empty( $block['className'] ) ) {
    $className .= ' ' . $block[ 'className' ] ;
}
if ( !empty( $block['align'] ) ) {
    $className .= 'align' . $block[ 'align' ] ;
}

$testimonialsBg = get_field( 'testimonials_background' );

?>
        
<section class="<?php echo esc_attr( $className ); ?>-section px-0 py-3 py-md-5 align-items-center bg-green"
    <?php 
    if ( $testimonialsBg ) {
        echo sprintf(
            'style="background: url(%1$s); background-size: cover; background-position: center; background-attachment: fixed;"',
            $testimonialsBg['url']
        );
    }
    ?>>
            
    <div class="container px-0">
        <div id="heroCarousel" class="carousel slide" data-ride="carousel" data-interval="8000">
            <ol class="carousel-indicators">
                <?php 
                $indicator = 0;
                $indicator_dot = '<li data-target="#heroCarousel" data-slide-to="%1$s" %2$s></li>';

                    while( have_rows('carousel') ): the_row();
                        // if ($indicator === 0) {
                        //     echo '<li data-target="#heroCarousel" data-slide-to="0" class="active"></li>';
                        // } else {
                        //     echo '<li data-target="#heroCarousel" data-slide-to="'.$indicator.'"></li>';
                        // }
                        if ($indicator === 0) {
                            echo sprintf(
                                $indicator_dot,
                                0,
                                'class="active"'
                            );
                        } else {
                            echo sprintf(
                                $indicator_dot,
                                $indicator,
                                ''
                            );
                        }
                    $indicator++;
                endwhile;
                ?>
            </ol>

            <div class="carousel-inner">

            <?php 
            $slide = 0;

            $testimonials = new WP_Query( array(
                'posts_per_page'    => 5,
                'post_type'         => 'testimonial'

            ));

            while( $testimonials->have_posts() ) :
                $testimonials->the_post();

                $exportTitle        = get_the_title();
                $exportLink         = get_the_content();
            ?>
            
            <div class="carousel-item <?php echo ( $slide === 0 ? 'active' : '' ); ?>">
                <div class="carousel-caption">
                    <div class="container px-0">
                        <div class="row align-items-center justify-content-center">

                            <div class="col-12 col-md-8">
                                <p class="mb-1" style="text-align: center;">We’re heading in the right direction…</p>
                                <?php echo $exportLink; ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <?php
            $slide++;
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
</section>

