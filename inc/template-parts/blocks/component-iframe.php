<?php 

    $className = 'iframe-content';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $iframeContent = get_field( 'iframe_content' );
    $iframeLocations = get_field( 'iframe_locations' );

?>

<style>

    #main-alt .container {
        max-width: unset !important;
    }

    #main-alt .row > div {
        padding: 0 !important;
    }

    #main-alt > .blog-header {
        margin-bottom: 0 !important;
    }

    iframe > html {
        padding: 0 !important;
    }

    #wprmenu_bar {
        display: none !important;
    }

</style>

<?php

        
    echo '
        <script>
            function resizeIframe(obj) {
                obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + "px";
            }
        </script>

        <iframe class="bdac-iframe d-none d-md-block" min-width="100vw"
        height="100vh"
        src="' . $iframeContent . '"
        scrolling="no" 
        onload="resizeIframe(this)"
        style="min-width: 100vw; min-height: 100vh; height: 1333px; border: none;"
        >
            
        </iframe>

        <div class="container d-block d-md-none py-5 py-md-0" style="max-width: 1380px">';

            // Check rows exists.
            if( have_rows( 'iframe_locations' ) ):
                // Loop through rows.
                while( have_rows( 'iframe_locations' ) ) : the_row();
                $iframeCount = 1;

                    // Load sub field value.
                    $iframeLocation = get_sub_field( 'iframe_location' );
                    $iframeEpu = get_sub_field( 'iframe_epu' );
                    $iframeCode = get_sub_field( 'iframe_code' );
                    $iframeBadge = get_sub_field( 'iframe_badge' );
                    // Do something...
                    echo '
                        <div class="bdac-iframe-locations">
                            <div class="bdac-iframe-locations-count">
                                <h3>' . get_row_index() . '</h3>
                            </div>
                            <div class="bdac-iframe-locations-details">
                                <h3>' . $iframeLocation . '</h3>
                                <p><strong>EPU:</strong> ' . $iframeEpu . '</p>
                                <p><strong>CODE:</strong> ' . $iframeCode . '</p>
                                <p><strong>BADGE:</strong> ' . $iframeBadge . '</p>
                            </div>
                        </div>
                    ';

                    $iframeCount++;
                endwhile;
            endif;            

    echo '    
        </div>
    ';


?>


