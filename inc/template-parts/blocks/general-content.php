<?php

    $className = 'general-content-block';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $general_title      = get_field( 'general_title' );
    $general_content    = get_field( 'general_content' );

    echo '
        <h3 class="general-content-title">' . $general_title . '</h3>

        <div class="intro-seperator d-flex flex-row justify-content-center align-items-center">
            <hr class="intro-seperator-hr">
            <img src="https://supremefreight.com/wp-content/themes/enfold-child/images/divider-icon.png" alt="" class="intro-seperator-icon">
            <hr class="intro-seperator-hr">
        </div>
            
        <p class="general-content-copy text-center">
            ' . $general_content . '
        </p>
    ';

    wp_reset_postdata();

?>
