<?php 

    $className = 'call2action-block';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $c2aPhone       = get_field( 'c2a_phone' );
    $c2aEmail       = get_field( 'c2a_email' );
    $c2aAnniversary = get_field( 'c2a_anniversary' );
    $c2aLangs       = get_field( 'c2a_languages' );
    $c2a_item       = '<p><i class="fas fa-%1$s"></i> %2$s</p>'
?>

        <section class="<?php echo esc_attr( $className ); ?>">
            <div class="call2action-contact">
                <div class="container">
                    <div class="row justify-content-center align-items-center">
                        <div class="call2action-contact-phone">
                            <?php 
                            if ( $c2aPhone ) {
                                echo sprintf(
                                    $c2a_item,
                                    'phone-alt',
                                    $c2aPhone
                                );
                            }
                            ?>
                            
                        </div>
                        <div class="call2action-contact-email">
                            <?php 
                            if ( $c2aEmail ) {
                                echo sprintf(
                                    $c2a_item,
                                    'envelope',
                                    $c2aEmail
                                );
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php 
                if ( $c2aAnniversary ) {
                    echo sprintf(
                        '<img class="call2action-contact-anniversary d-none d-sm-block" src="%1$s" alt="%2$s">',
                        $c2aAnniversary['url'],
                        $c2aAnniversary['alt']
                    );
                }
                ?>
                </div>
                <div class="call2action-languages">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                        <?php 
                        if( have_rows( 'c2a_languages' ) ):
                            while( have_rows( 'c2a_languages' ) ) : the_row();
        
                                $c2aLangText = get_sub_field('c2a_language_text');
                                $c2aLangIcon = get_sub_field('c2a_language_icon');
                        ?>
                        <div class="d-flex flex-row call2action-languages-language justify-content-center align-items-center">
                            
                            <?php 
                            echo sprintf(
                                '<img src="%1$s" alt="%2$s">',
                                $c2aLangIcon['url'],
                                $c2aLangIcon['alt']
                            );
                            echo sprintf(
                                '<p>%1$s</p>',
                                $c2aLangText
                            );
                            ?>
                        </div>
                        <?php
                            endwhile;
                        endif
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>    
                    
                    
