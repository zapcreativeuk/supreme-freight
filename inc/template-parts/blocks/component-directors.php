<?php
    echo '
        <section class="block-directors">
            <div class="container">
                <div class="row">
                ';
                
                $personnel = new WP_Query( array(
                    'posts_per_page'    => 4,
                    'post_type'         => 'personnel',
                    'orderby'           => 'date',
                    'order'             => 'ASC'

                ));

                while( $personnel->have_posts() ) :
                    $personnel->the_post();

                    $directorName       = get_the_title();
                    $directorExcerpt    = get_the_excerpt();
                    $directorRole       = get_field( 'personnel_role' );
                    $directorBio        = get_the_content();
                    $dirString          = str_replace(' ', '', $directorName);
                    
                    echo '
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
                            <h3 class="block-directors-title">' . $directorName . '</h3>
                            <p class="block-directors-role">' . $directorExcerpt . '</p>
                            <button class="btn btn-supreme" data-toggle="modal" data-target="#' . $dirString . 'Modal">Read More</button>
                        </div>
                        <div class="modal fade" id="' . $dirString . 'Modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body p-5">
                                    <h4 class="modal-title" id="exampleModalLabel">' . $directorName . '</h4>
                                    <p>' . $directorExcerpt . '</p>
                                    ' . supremeFreightBreak() . 
                                        $directorBio . 
                                        wp_reset_postdata() . '
                                </div>
                            </div>
                        </div>
                    </div>
                    ';

                wp_reset_postdata();
                endwhile;

        echo '
            </div>
        </div>
    </section>
    ';

?>
