<?php 

    $className = 'blog';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $introHeader    = get_field( 'intro_header' );
    $introSubHeader = get_field( 'intro_sub-header' );

    echo '
        <section class="' . esc_attr( $className ) . '-section">

            <div class="container">
                <div class="row justify-content-center py-3 py-md-5">
                    <h2>Latest News</h2>'
                        . supremeFreightBreak();

                        $getBlog = array(
                            'posts_per_page' => 3
                        );
                        $blogQuery = new WP_Query( $getBlog );

                        while ( $blogQuery->have_posts() ) {
                            $blogQuery->the_post();

                            $postTitle      = get_the_title();
                            $postLink       = get_the_permalink();
                            $postDate       = get_the_time('F j, Y');
                            $postTags       = '';
                            $postExcerpt    = wp_trim_words(get_the_excerpt(), 40);

                            echo '
                                <div class="col-md-4 d-flex flex-column">
                                    <h3><a href="' . $postLink . '">' . $postTitle . '</a></h3>
                                    <small>' . $postDate . '</small>
                                        <p class="blog-section-tags">';
                                            
                                            if ($postTags) {

                                                echo '<strong>Tags</strong>:';
                                                
                                                foreach(get_the_tags() as $tag) {
                                                    echo '
                                                        <a class="blog-post-header-categories-category" href="' . $tag->link . '">' . $tag->slug . '</a>,
                                                    ';
                                                };
                                            } else {
                                                echo '';
                                            };

                                echo '
                                        </p>
                                        <p class="blog-section-categories">
                                            <strong>Categories</strong>:';
                                            foreach(get_the_category() as $category) {
                                                echo '
                                                    <a class="blog-post-header-categories-category" href="' . $category->link . '">' . $category->slug . '</a>,
                                                ';
                                            };
                                echo '
                                    <hr>
                                    <p>' . $postExcerpt . '</p>
                                    <a href="' . $postLink . '" class="btn btn-supreme mt-auto" style="width: fit-content;">Read More</a>
                                </div>';
                        };
            echo '   
                </div>
            </div>
        </section>
    ';

?>