<?php 

    $className = 'export';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $exportBlockTitle   = get_field( 'export_services_title' );
    $exportBlockButton  = get_field( 'export_services_button' );
    $exportBlockLink    = get_field( 'export_services_link' );

        echo '
            <section class="' . esc_attr( $className ) . '-section">
                <div class="container">
                    
                    <div class="row justify-content-center">
                        <h2 class="intro-title text-center">' . $exportBlockTitle . '</h2>
                        ' . supremeFreightBreak() . '
                    </div>

                    <div class="row py-3">

                    ';

                    $exportServices = new WP_Query( array(
                        'posts_per_page'    => 12,
                        'post_type'         => 'export',
                        'orderby'           => 'date',
                        'order'             => 'ASC'

                    ));

                    while( $exportServices->have_posts() ) :
                        $exportServices->the_post();

                        $exportTitle        = get_the_title();
                        $exportLink         = get_permalink();
                        $exportContent      = get_the_excerpt();
                        $postImg            = get_the_post_thumbnail_url();
                        $exportFrontIcon    = get_field( 'export_front_page_icon' );
                        $exportFrontCaption = get_field( 'export_front_page_caption' );
                        
                        echo '
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
                                <a href="' . $exportLink . '">
                                    <img src="' . ( $postImg ? $postImg : 'https://supremefreight.com/wp-content/uploads/2016/09/sea_export.png' ) . '" alt="' . $exportFrontIcon['alt'] . '">
                                </a>
                                <div class="export-section-content">
                                    <a href="">
                                        <h4>' . $exportTitle . '</h4>
                                    </a>
                                    <p>' . $exportContent . '</p>
                                </div>
                            </div>
                        ';
                    
                    endwhile;
                    wp_reset_postdata();
                
    echo '
                    </div>

                    ' . ( 
                        $exportBlockButton && $exportBlockLink ? 
                        '
                            <div class="row justify-content-center align-items-center mt-3 mt-md-5">
                                <a class="btn btn-supreme" href="' . $exportBlockLink . '">' . $exportBlockButton . '</a>
                            </div>
                        ' : 
                        ''
                    ) . '

                </div>
            </section>
        ';

?>

