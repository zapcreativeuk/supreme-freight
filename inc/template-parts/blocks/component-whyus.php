<?php

    $className = 'why-us-block';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $why_repeat = get_field( 'why_us' );

    echo '
        <section class="' . esc_attr( $className ) . '">
            <div class="container">
                <div class="row">
                    ';
                    
                    if( have_rows( 'why_us' ) ):
                        while( have_rows( 'why_us' ) ) : the_row();

                            // Repeater variables
                            $why_title          = get_sub_field('why_title');
                            $why_img            = get_sub_field('why_image');
                            $why_description    = get_sub_field('why_description');

                            echo '
                                <div class="why-us-card col-12 col-md-6 col-lg-4">
                                    <img class="why-us-card-img" src="' . $why_img['url'] . '" alt="' . $why_img['alt'] . '" />
                                    <h4 class="why-us-card-title">' . $why_title . '</h4>
                                    <p class="why-us-card-copy text-center">' . $why_description . '</p>
                                    
                                </div>
                            ';
                        endwhile;
                    endif;

                echo '
                </div>
            </div>
        </section>
    ';

    wp_reset_postdata();

?>
