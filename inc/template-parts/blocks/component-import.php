<?php 

    $className = 'import';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $importBlockTitle   = get_field( 'import_services_title' );
    $importBlockButton  = get_field( 'import_services_button' );
    $importBlockLink    = get_field( 'import_services_link' );
    

    echo '
        <section class="' . esc_attr( $className ) . '-section">
            <div class="container">
                
                <div class="row justify-content-center">
                    <h2 class="intro-title text-center">' . $importBlockTitle . '</h2>
                    ' . supremeFreightBreak() . '
                </div>

                <div class="row py-3">
    ';

                    $importServices = new WP_Query( array(
                        'posts_per_page'    => 12,
                        'post_type'         => 'import',
                        'orderby'           => 'date',
                        'order'             => 'ASC'

                    ));

                    while( $importServices->have_posts() ) :
                        $importServices->the_post();

                        $importTitle        = get_the_title();
                        $importLink         = get_the_permalink();
                        $importContent      = get_the_excerpt();
                        $postImg            = get_the_post_thumbnail_url();
                        $importIcon         = get_field( 'import_icon' );
                        $importIconUrl      = get_field( 'import_icon_url' );
                        $importFrontCaption = get_field( 'import_content' );
                        
                        echo '
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
                                <a href="' . $importLink . '">
                                    <img src="' . ( $postImg ? $postImg : 'https://supremefreight.com/wp-content/uploads/2016/09/sea-import.png' ) . '" alt="' . $importIcon['alt'] . '">
                                </a>
                                <div class="import-section-content">
                                    <a href="">
                                        <h4>' . $importTitle . '</h4>
                                    </a>
                                    <p>' . $importContent . '</p>
                                    <p>' . $importIconUrl . '</p>
                                </div>
                            </div>
                        ';
                    
                    endwhile;
                    wp_reset_postdata();
                
        echo '
                    </div>


                    ' . ( 
                        $importBlockButton && $importBlockLink ? 
                        '
                            <div class="row justify-content-center align-items-center mt-3 mt-md-5">
                                <a class="btn btn-supreme" href="' . $importBlockLink . '">' . $importBlockButton . '</a>
                            </div>
                        ' : 
                        ''
                    ) . '
                </div>
            </section>
            ';

    


