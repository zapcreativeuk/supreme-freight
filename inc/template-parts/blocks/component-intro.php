<?php 

    $className = 'intro-block';
    if ( !empty( $block['className'] ) ) {
        $className .= ' ' . $block[ 'className' ] ;
    }
    if ( !empty( $block['align'] ) ) {
        $className .= 'align' . $block[ 'align' ] ;
    }

    // Variables
    $introHeader    = get_field( 'intro_header' );
    $introSubHeader = get_field( 'intro_sub-header' );
    $introDesc      = get_field( 'intro_description' );
    $introLead      = get_field( 'intro_copy' );
    $introBtn       = get_field( 'intro_button_link' );
    $introBtnLabel  = get_field( 'intro_button_label' );

    echo '
        <section class="' . esc_attr( $className ) . '">
            <div class="container">
                <div class="row justify-content-center align-items-center text-center">
                    <h2 class="intro-title">' . $introHeader . ' ' . ( !is_front_page() ? '' : supremeFreightAge() . ' years.' ) . '</h2>
                    ' . supremeFreightBreak() . '
                    <h3 class="intro-sub-title">' . $introSubHeader . '</h3>
                    <p class="lead">' . $introDesc . ' ' . ( !is_front_page() ? supremeFreightAge() . ' years.' : '' ) .'</p>
                    <p>' . $introLead . '</p>' . 
                        ( 
                            $introBtn && $introBtnLabel ?
                            '<a class="btn btn-supreme" href="' . $introBtn . '">' . $introBtnLabel . '</a>' :
                            ''
                        ) . 
                '</div>
            </div>
            ';

        if( have_rows('intro_accreditations') ):
        echo '
            <div class="container">
                <div class="intro-accreditaion">
                    <div class="row flex-column justify-content-center align-items-center">
                        <h3 class="">PROUD TO BE ACCREDITED WITH:</h3>
                    </div>
                <div class="intro-accreditaion-logos row text-center">
                ';
            while( have_rows('intro_accreditations') ) : the_row();
                $introAccred = get_sub_field('intro_accreditation_logo');
                echo '
                    <div class="intro-accreditaion-logos-logo col-6 col-md-1">
                        <img src="' . $introAccred['url'] . '" alt="' . $introAccred['alt'] . '" class="intro-accreditaion-logo">
                    </div>
                ';
            endwhile;
            echo '
                </div>
            </div>';
        endif;      
        echo '
            </div>
        </section>
    ';
?>

