<?php

    get_header();

    $about_title = get_the_title();
    $about_thumb = get_the_post_thumbnail_url();

    echo '
        <section class="about-header align-items-end" style="background-image: url(' . $about_thumb . ');">
        
            <h1 class="about-header-title">' . $about_title . '</h1>

        </section>
    ';

    while( have_posts() ) :
        the_post();    
            the_content();                    
    endwhile;

    echo '
        <section class="about-joining p-5">
            <div class="container">
                <div class="row justify-content-center">
                    <h3 class="mt-auto">Interested in joining our team?</h3>'

                    . supremeFreightBreak() .

                '
                    <p class="mb-auto">If you’re looking for another role or feel you have something you can offer please email <a href="mailto:enquiries@supremefreight.com">enquiries@supremefreight.com</a></p>
                </div>
            </div>
        </section>
    ';

    

    get_footer();

?>