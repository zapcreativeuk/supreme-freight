<?php
/**
 * Header file for the Supreme Freight v-2 Dev theme.
 */

?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!-- Global site tag (gtag.js) - Google Ads: 863801358 -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=AW-863801358"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-863801358');
      </script>

      <script>
        gtag('config', 'AW-863801358/f0wYCIq7j5IBEI6g8psD', {
          'phone_conversion_number': '02380337778'
        });
      </script>
	    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>

    <header id="header" class="header">
      <!-- Header -->
      <nav id="nav" class="navbar <?php  ?>navbar-light <?php echo ( !is_front_page() ? 'navbar-not-front' : 'navbar-scrolled'  ); ?> fixed-top navbar-expand-lg">
        
          <a class="navbar-brand logo-container" href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <?php echo supremeFreightLogo(); ?>
          </a>
          <button class="navbar-toggler navbar-light ml-auto order-12" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

            <?php 
              wp_nav_menu( array(
                'theme_location'  => 'mainNav',
                'container'       => 'nav',
                'container_id'    => 'navbarSupportedContent',
                'container_class' => 'collapse navbar-collapse',
                'menu_class'      => 'navbar-nav ml-auto',
              ) ); 
            ?>
            <li class="btn btn-supreme mx-0 nav-get-quote">
              <a href="#" data-toggle="modal" data-target="#quoteModal">Get a Quote</a>
            </li>
      </nav>

      <div class="modal fade" id="quoteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-header-title">GET A QUOTATION NOW</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                  </button>

              </div>
              <div class="modal-body p-5">
                <?php echo do_shortcode( '[contact-form-7 id="386" title="Main Contact Form"]', false ) ?>
              </div>
          </div>
      </div>
    </header>
    
    <main id="main<?php echo ( !is_front_page() ? '-alt' : ''  ); ?>" >
