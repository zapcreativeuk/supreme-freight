<?php
/**
 * Index file for the Supreme Freight SF034.
 */

    get_header();
    $page_title = get_the_title( get_option('page_for_posts', true) );;
    
    echo  
        supremeFreightPageTitleBanner( $page_title ) . '
        <div class="container">
        
    ';
    
    while( have_posts() ) :
        the_post();
        
        $post_title         = get_the_title();
        $post_content       = wp_trim_words( get_the_content(), 55 );
        $post_thumb         = get_the_post_thumbnail_url();
        $post_link          = get_the_permalink();
        $post_author_link   = 'some';
        $post_date          = get_the_date('F j, Y');

        echo '
            <article class="blog-post" style="">
                <a href="'. $post_link . '" class="blog-post-img">
                    <img src="' . $post_thumb . '" class="blog-post-img-image">
                    <div class="blog-post-img-overlay">
                        <h2><i class="fas fa-chevron-circle-right"></i></h2>
                    </div>
                </a>
                <div class="blog-post-header">
                    <a href="' . $post_author_link . '" class="blog-post-header-author">
                        ' . supremeFreightIcon() . '
                    </a>
                    <a href="'. $post_link . '" class="blog-post-header-title text-center">
                        <h1 class="blog-post-header-title-text">' . $post_title . '</h1>
                    </a>

                    ' . supremeFreightBreak() . '

                    <div class="blog-post-header-meta">
                        <p class="blog-post-header-meta-date">' . $post_date . '<span> / </span></p>
                        <ul class="blog-post-header-meta-categories">
                            <li class="blog-post-header-meta-categories-category">in </li>
                            ';
                                
                                foreach(get_the_category() as $category) {
                                    echo '
                                    <li class="blog-post-header-meta-categories-category">
                                        <a href="' . $category->link . '">' . $category->slug . '</a><span>,</span>
                                    </li>
                                    ';
                                };
                    echo '
                        </ul>
                    </div>
                </div>

                <div class="blog-post-wrapper">
                    <p>' . $post_content . '</p>
                    <a href="' . $post_link . '">
                        <button class="btn btn-supreme blog-post-wrapper-button">Read More</button>
                    </a>
                </div>
                <hr>
            </article>
        '; 
    endwhile;
    wp_reset_postdata();

    echo '
            <nav class="blog-pagination">
                <div class="container ">
                    <div class="row">
                        <div class="col-sm-6">'

                            . paginate_links() . 
                
                        '</div>
                        <div class="col-sm-6">
                        
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    ';

    get_footer();
?>

<!-- supremeFreightIcon() -->