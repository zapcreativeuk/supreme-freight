<?php

    get_header();

    echo '
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <p class="entry-content"><strong>Nothing Found</strong><br>
                    Sorry, the post you are looking for is not available. Maybe you want to perform a search?</p>';
                    
                echo dynamic_sidebar( 'page_404' );
                        
                echo '
                    <p>For best search results, mind the following suggestions:</p>
                    <ul class="search-list">
                        <li>Always double check your spelling.</li>
                        <li>Try similar keywords, for example: tablet instead of laptop.</li>
                        <li>Try using more than one keyword.</li>
                    <ul>
                </div>
                <div class="col-md-4">
                </div>
            
            </div>
        </div>
    ';

    get_footer();

?>



