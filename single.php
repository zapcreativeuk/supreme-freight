<?php
/**
 * Index file for the Supreme Freight SF034.
 */

    get_header();
    
    while( have_posts() ) :
        the_post();
        
        $post_title         = get_the_title();
        $post_content       = get_the_content();
        $post_thumb         = get_the_post_thumbnail_url();
        $post_link          = get_the_permalink();
        $post_date          = get_the_date('F j, Y');
        $post_author_link   = get_the_author_url();
        
        echo '
        ';
    endwhile;
    wp_reset_postdata();
    
    echo '
        <div class="container">
            <div class="row">
                <main class="blog-post col-12 col-md-8">
                    <a href="'. $post_link . '" class="blog-post-img">
                        <img src="' . $post_thumb . '" class="blog-post-img-image">
                    </a>
                    <div class="blog-post-container">
                        <h1 class="blog-post-title">' . $post_title . '</h1>
                        <div class="blog-post-header-meta">
                            <p class="blog-post-header-meta-date">' . $post_date . '<span> / </span></p>
                            <ul class="blog-post-header-meta-categories">
                                <li class="blog-post-header-meta-categories-category">in </li>
                        ';
                            
                                    foreach(get_the_category() as $category) {
                                        echo '
                                        <li class="blog-post-header-meta-categories-category">
                                            <a href="' . $category->link . '">' . $category->slug . '</a>
                                        </li>
                                        ';
                                    };
                        echo '
                            </ul>
                        </div>
                        <div class="blog-post-content">
                            ' . $post_content . '
                        </div>
                    </div>
                </main>
                <aside class="blog-sidebar col-12 col-md-4">
            ';

            dynamic_sidebar( 'single_sidebar' );
                
        echo '
                </aside>
            </div>
        </div>';
    
    get_footer();

?>
