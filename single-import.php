<?php 

    get_header();

    $postTitle      = get_the_title();
    $postThumb      = get_field( 'import_icon' ); 
    $postIcon       = get_the_post_thumbnail_url( );
    $postHeadline   = get_field( 'import_title' );
    $postContent    = get_field( 'import_content' );
    $postButton     = get_field( 'import_button_label' );
    $postLink       = get_field( 'import_button_link' );

    echo '

        <div class="single-import-hero">
            <img src="' . $postThumb['url'] . '" alt="" class="single-import-hero-background" style="max-width: 100vw;">
            <div class="single-import-hero-overlay">
                <img src="' . $postIcon . '" alt="" class="single-import-hero-icon">
                <p class="single-import-hero-overlay-desc">SUPREME IMPORT SERVICES:</p>
                <h1 class="single-import-hero-title">' . $postTitle . '</h1>
            </div>
        </div>
        <div class="container">
            <div class="row ">
                <main class="col-12 col-md-8 single-import-main">
                    <h2 class="single-import-main-title">' . $postHeadline . '</h2>
                    ' . supremeFreightBreak() . '
                    <p>' . $postContent . '<p>
                    ' . supremeFreightBreak() . '
                    <a href="' . ( $postLink ? $postLink : '/contact' ) . '">
                        <button class="btn btn-supreme single-import-main-button">
                            ' . $postButton . '
                        </button>
                    </a>
                </main>
                <aside class="col-12 col-md-4 single-import-aside">
                    <h3 class="single-import-aside-title">Supreme Import Services</h3>';

                        $importServices = new WP_Query( array(
                            'posts_per_page'    => -1,
                            'post_type'         => 'import',
                            'orderby'           => 'date',
                            'order'             => 'ASC'

                        ));

                        while( $importServices->have_posts() ) :
                            $importServices->the_post();

                            $importListTitle    = get_the_title();
                            $importLink         = get_the_permalink();
                            $importListIcon     = 'https://supremefreight.com/wp-content/uploads/sidebar-li-arrow.png';
                            
                            echo '
                                <a class="single-import-aside-list-item-link" href="' . $importLink . '">
                                    <img class="single-import-aside-list-item-icon" src="' . $importListIcon . '">' . $importListTitle .'
                                </a>
                            ';
                        
                        endwhile;
                        wp_reset_postdata();

            echo '
                </aside>
            </div>
        </div>
    ';


    get_footer();

?>

