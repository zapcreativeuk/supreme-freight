<?php 

    get_header();

    $postTitle = get_the_title();
    $postThumb = get_the_post_thumbnail_url( );
    $postIcon = get_field( 'export_image' );
    $postHeadline = get_field( 'export_title' );
    $postContent = get_field( 'export_content' );
    $postButton     = get_field( 'export_button_label' );
    $postLink       = get_field( 'export_button_link' );

    echo '
        <style>
            
        </style>

        <div class="single-export-hero">
            <img src="' . $postIcon['url'] . '" alt="" class="single-export-hero-background" style="max-width: 100vw;">
            <div class="single-export-hero-overlay">
                <img src="' . $postThumb . '" alt="" class="single-export-hero-icon">
                <p class="single-export-hero-overlay-desc">SUPREME EXPORT SERVICES:</p>
                <h1 class="single-export-hero-title">' . $postTitle . '</h1>
            </div>
        </div>
        <div class="container">
            <div class="row ">
                <main class="col-12 col-md-8 single-export-main">
                    <h2 class="single-export-main-title">' . $postHeadline . '</h2>
                    ' . supremeFreightBreak() . '
                    <p>' . $postContent . '<p>
                    ' . supremeFreightBreak() . '
                    <a href="/contact">
                    <a href="' . ( $postLink ? $postLink : '/contact' ) . '">
                        <button class="btn btn-supreme single-export-main-button">
                            ' . $postButton . '
                        </button>
                    </a>
                    </a>
                </main>
                <aside class="col-12 col-md-4 single-export-aside">
                    <h3 class="single-export-aside-title">Supreme export Services</h3>';

                        $exportServices = new WP_Query( array(
                            'posts_per_page'    => -1,
                            'post_type'         => 'export',
                            'orderby'           => 'date',
                            'order'             => 'ASC'

                        ));

                        while( $exportServices->have_posts() ) :
                            $exportServices->the_post();

                            $exportListTitle    = get_the_title();
                            $exportLink         = get_the_permalink();
                            $exportListIcon     = 'https://supremefreight.com/wp-content/uploads/sidebar-li-arrow.png';
                            
                            echo '
                                <a class="single-export-aside-list-item-link" href="' . $exportLink . '">
                                    <img class="single-export-aside-list-item-icon" src="' . $exportListIcon . '">' . $exportListTitle .'
                                </a>
                            ';
                        
                        endwhile;
                        wp_reset_postdata();

            echo '
                </aside>
            </div>
        </div>
    ';


    get_footer();

?>

